import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'first-app';

  contador: number=0;
  
  
  animales: Array<any> =[
    {Tipo: 'Perro', Nombre: 'Mia', Edad: 2},
    {Tipo: 'Gato', Nombre: 'Miauuuu', Edad: 5},
    {Tipo: 'Perro', Nombre: 'Danger', Edad: 4},
  ]

}
